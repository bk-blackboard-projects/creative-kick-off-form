var contactNameEmail = "Meghan Hawk (meghan.hawk@blackboard.com)";
var eventsBound = false;

$(function() {
	// FIREBASE CONFIGURATION
	var creativeSchedulingApp = csFirebaseConfig("AIzaSyBsd5VapNbRM1W-rJpl-YRRyVpgPdA2L1A", "cs-kick-off-form.firebaseapp.com", "https://cs-kick-off-form.firebaseio.com", "");
	
	// AUTHENTICATION
	creativeSchedulingApp.auth().onAuthStateChanged(function(user) {
    	if(user && !user.isAnonymous) {
			buildView(creativeSchedulingApp);
			
			$(".section.main.loading").removeClass("loading");
			$("#login-form").addClass("hidden");
			$("#settings").removeClass("hidden");
		} else {
			$(".section.main.loading").removeClass("loading");
			$("#settings").addClass("hidden");
			$("#login-form").removeClass("hidden");

			// BIND EVENTS
			loginEvents(creativeSchedulingApp);
			lightBoxEvents();
		}
    });
});

function csFirebaseConfig(apiKey, authDomain, databaseURL, storageBucket) {
	// FIREBASE CONFIGURATION
	var config = {
		apiKey: apiKey,
		authDomain: authDomain,
		databaseURL: databaseURL,
		storageBucket: storageBucket
	};
	
	// INITIALIZE THE FIREBASE
	var csFirebaseApp = firebase.initializeApp(config);
	
	// RETURN THE FIREBASE INSTANCE
	return csFirebaseApp;
}

function getData(kickOffDB) {
	// BUILD THE VIEW AND SET AN ASYNCHRONOUS CALLBACK TO KEEP VIEW IN SYNC WITH DATA AT ALL TIMES
	kickOffDB.on("value", function(snapshot) {
		// REBUILD THE FORM WITH THE NEW DATA
		buildView(kickOffDB, snapshot);
	}, function (error) {
		if(error) {
			// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
			lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The following error has occured:<br /><span style='color: #990000;'>" + error.code + ".</span><br />If you continue to receive this error, please contact " + contactNameEmail + " with the error details.</p>");
		}
	});
}

function buildView(app) {
	// GET THE SUBMISSIONS
	app.database().ref("/formSubmissions").once("value", function(snapshot) {
		var kickOffs = snapshot.val();
		
		// DELETE OLD SUBMISSIONS
		var todayDate = new Date();
		for(var i in kickOffs) {
			var kickOffDate = new Date(parseInt(kickOffs[i].epochTime));

			// IF THE SUBMISSION IS IN THE PAST
			if(kickOffDate < todayDate) {
				app.database().ref("/formSubmissions/" + i).remove().catch(function(error) {
					// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
					console.log("Uh oh! The following error has occured while trying to delete old form submissions: " + error.code + ".");
				});
			}
		}
		
		// SUBMISSIONS
		if(!$.isEmptyObject(kickOffs)) {
			var submissions = "";
			for(var s in kickOffs) {
				var kickOffDate = new Date(parseInt(kickOffs[s].epochTime));

				// IF THE SUBMISSION IS NOT IN THE PAST
				if(kickOffDate > todayDate) {
					submissions += '<li class="submission">' + 
						'<span class="submission-info">' +
							kickOffs[s].date + ', ' + kickOffs[s].organization + ', ' + kickOffs[s].fullName + ' (' + kickOffs[s].email + ')' +
							((kickOffs[s].notes !== undefined && kickOffs[s].notes.length) ? '<span class="notes">Notes: <span>' + kickOffs[s].notes.replace(/\\"/g, "&quot;") + '</span></span>' : '') +
						'</span>' +
						'<button class="delete-kickoff" data-kickoff-id="' + s + '"><i class="fa fa-times"></i></button>' +
					'</li>';	
				}
			}
			$("#submission-list").html(submissions);
		}
	}, function(error) {
		console.log(error);
	});
	
	// GET THE SETTINGS
	app.database().ref("/settings").once("value", function(snapshot) {
		var settings = snapshot.val();

		// APPLY CURRECT SETTINGS
		// NUMBER OF DAYS INTO THE FUTURE TO GENERATE DATES/TIMES
		$("#days-into-future").val(settings.futureDays);

		// NUMBER OF DAYS INTO THE FUTURE WHEN DATES/TIMES START
		$("#days-to-expire").val(settings.daysOutToExpire);

		// NUMBER OF SLOTS PER TIME
		$("#num-of-slots").val(settings.numberOfSlots);

		// DATES TO EXCLUDE
		if(settings.excludeDays != "") {
			var excludedDates = settings.excludeDays.split(","), dateList = "";
			for(var d in excludedDates) {
				dateList += '<li class="excluded-date">' + excludedDates[d] + '<button class="delete-excluded-date"><i class="fa fa-times"></i></button></li>';
			}
			$("#excluded-dates").html(dateList);
		}

		// MEETING TIMES
		var slotTimesTwelveHour = ["8:00 AM", "9:00 AM", "10:00 AM", "11:00 AM", "12:00 PM", "1:00 PM", "2:00 PM", "3:00 PM", "4:00 PM", "5:00 PM"];
		var slotTimesTwentyFourHour = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
		if(settings.meetingTimes != "") {
			var meetingTimes = settings.meetingTimes.split(","), meetingTimeList = "";
			for(var t in meetingTimes) {
				var timePos = slotTimesTwentyFourHour.indexOf(parseInt(meetingTimes[t]));
				meetingTimeList += '<li class="meeting-time">' + slotTimesTwelveHour[timePos] + '<button class="delete-meeting-time"><i class="fa fa-times"></i></button></li>';	
			}
			$("#settings-form .meeting-times").html(meetingTimeList);
		}

		// GENERATE LIST OF MEETING TIMES
		var meetingTimeOptions = "";
		for(var slot in slotTimesTwelveHour) {
			meetingTimeOptions += '<li><a href="#" lass="meeting-time-option">' + slotTimesTwelveHour[slot] + '</a></li>';
		}
		$(".meeting-time-options, .specific-meeting-time-options").html(meetingTimeOptions);

		// SPECIFIC DATE/TIME SLOTS
		if(settings.adjustSpecificSlots != "") {
			var specificSlots = settings.adjustSpecificSlots.split(";"), specificSlotsList = "";
			for(var s in specificSlots) {
				specificSlotsList += '<li class="specific-date">' + specificSlots[s] + '<button class="delete-specific-date"><i class="fa fa-times"></i></button></li>';	
			}

			$("#specific-dates").html(specificSlotsList);
		}

		// EMAIL ADDRESSES
		$("#emails").val(settings.emails);
		
		// BIND EVENTS
		if(!eventsBound) {
			settingsEvents(app, meetingTimeOptions, slotTimesTwelveHour, slotTimesTwentyFourHour);
			lightBoxEvents();

			eventsBound = true;
		}
	}, function(error) {
		console.log(error);
	});
}

function loginEvents(db, kickOffDB) {
	// SUBMIT LOGIN FORM
	$("#login-form").submit(function(e) {
		e.preventDefault();
		
		// DO SOME VALIDATION
		var hasError = false, valErrors = "";
		$(".login-input.required").each(function() {
			if($.trim($(this).val()) == "" || $.trim($(this).val()) == $(this).attr("data-error-message")) {
				$(this).addClass("is-empty");
				
				valErrors += '<li>' + $(this).attr("data-error-message") + '</li>';
				hasError = true;
			}
		});
		
		if(!hasError) {
			// AUTHENTICATE THE USER
			authentication(db, "login", $.trim($("#username").val()), $.trim($("#password").val()));
		} else {
			// SHOW ERROR MESSAGE
			lightBox("<h2>Uh oh!</h2><ul class='light-box-message'>" + valErrors + "</ul>");	
		}
	});	
	
	// FORM INPUT FOCUS
	$(".login-input").focus(function() {
		if($.trim($(this).val()) == "" || $.trim($(this).val()) == $(this).attr("data-error-message")) {
			$(this).val("").removeClass("is-empty");
		}
	});
}

function settingsEvents(app, meetingTimeOptions, slotTimesTwelveHour, slotTimesTwentyFourHour) {
	// BIND DATEPICKER
	$(".date-picker").datepicker();
	
	// ADD EXCLUDED DATE
	$("#add-exclude-date").click(function(e) {
		e.preventDefault();
		if($("#exclude-date-picker").val() != "") {
			$("#excluded-dates").append('<li class="excluded-date">' + $("#exclude-date-picker").val() + '<button class="delete-excluded-date"><i class="fa fa-times"></i></button></li>');
			
			$("#exclude-date-picker").val("");
		}
	});
	
	// REMOVE EXCLUDED DATE
	$(document).on("click", ".delete-excluded-date", function(e) {
		e.preventDefault();
		
		$(this).parent().remove();
	});
	
	// OPEN TIME PICKER
	$(document).on("focus", ".meeting-time-picker", function() {
		$(this).parent().find(".meeting-time-options").addClass("open");
	});
	$("#light-box-utility-content").on("focus", ".specific-meeting-time-picker", function() {
		$("~ .specific-meeting-time-options", this).addClass("open");
	});
	
	// CHOOSE TIME
	$(document).on("click", ".meeting-time-options li a", function(e) {
		e.preventDefault();
		
		$(this).parents("label").find(".meeting-time-picker").val($(this).text());
		$(this).parents(".meeting-time-options").removeClass("open");
	});
	$("#light-box-utility-content").on("click", ".specific-meeting-time-options li a", function(e) {
		e.preventDefault();
		
		$(this).parents("label").find(".specific-meeting-time-picker").val($(this).text());
		$(this).parents(".specific-meeting-time-options").removeClass("open");
	});
	
	// CLOSE TIME PICKER
	$(document).click(function() {
		$(".meeting-time-options").removeClass("open");
		$(".specific-meeting-time-options").removeClass("open");
	});
	$(document).on("click", ".meeting-time-picker, .specific-meeting-time-picker, .meeting-time-options, .specific-meeting-time-options", function(e) {
		e.stopImmediatePropagation();
	});
	
	// ADD MEETING TIME
	$(document).on("click", ".add-meeting-time", function(e) {
		e.preventDefault();
		
		if($(this).prev(".meeting-time-picker").val() != "") {
			$(this).parent().next(".meeting-times").append('<li class="meeting-time">' + $(this).prev(".meeting-time-picker").val() + '<button class="delete-meeting-time"><i class="fa fa-times"></i></button></li>');
			
			$(this).prev(".meeting-time-picker").val("");
		}
	});
	$(".add-specific-meeting-time").click(function(e) {
		e.preventDefault();
		
		if($(this).prev(".specific-meeting-time-picker").val() != "") {
			$(".specific-meeting-times").append('<li class="specific-meeting-time">' + $(this).prev(".specific-meeting-time-picker").val() + '<button class="delete-specific-meeting-time"><i class="fa fa-times"></i></button></li>');
			
			$(this).prev(".specific-meeting-time-picker").val("");
		}
	});
	
	// REMOVE MEETING TIME
	$(document).on("click", ".delete-meeting-time", function(e) {
		e.preventDefault();
		
		$(this).parent().remove();
	});
	$("#light-box-utility-content").on("click", ".delete-specific-meeting-time", function(e) {
		e.preventDefault();
		
		$(this).parent().remove();
	});
	
	// SPECIFIC DATE ADD LIGHTBOX
	$("#specific-date-lightbox").click(function(e) {
		e.preventDefault();
		
		$("#light-box-utility-outer").fadeIn();
	});
	
	// ADD SPECIFIC TIME
	$("#light-box-utility-close").click(function(e) {
		e.preventDefault();
		$("#specific-start-date-picker, #specific-end-date-picker, #specific-meeting-time-picker, #specific-num-of-slots").removeClass("is-empty");
		
		var hasError = false;
		if($("#specific-start-date-picker").val() == "") {
			$("#specific-start-date-picker").addClass("is-empty");
			hasError = true;
		}
		if($("#specific-end-date-picker").val() == "") {
			$("#specific-end-date-picker").addClass("is-empty");
			hasError = true;
		}
		if(!$(".specific-meeting-times li").length) {
			$("#specific-meeting-time-picker").addClass("is-empty");
			hasError = true;
		}
		if($("#specific-num-of-slots").val() == "") {
			$("#specific-num-of-slots").addClass("is-empty");
			hasError = true;
		}
		
		if(!hasError) {
			$("#light-box-utility-outer").fadeOut(function() {
				$("#specific-start-date-picker").val("");
				$("#specific-end-date-picker").val("");
				$("#specific-meeting-time-picker").val("");
				$(".specific-meeting-times").empty();
				$("#specific-num-of-slots").val("");
			});
			
			var timeSlots = "";
			$(".light-box-message .specific-meeting-times li").each(function() {
				timeSlots += $(this).text();
				if(!$(this).is(":last-child")) {
					timeSlots += ', ';	
				}
			});
			
			$("#specific-dates").append('<li class="specific-date">' + $("#specific-start-date-picker").val() + ' - ' + $("#specific-end-date-picker").val() + ' | Time Slots: ' + timeSlots + ' | Number of Time Slots: ' + $.trim($("#specific-num-of-slots").val()) + '<button class="delete-specific-date"><i class="fa fa-times"></i></button></li>');
		} else {
			return false;	
		}
	});
	
	// LIGHTBOX UTILITY INPUT FOCUS
	$("#specific-start-date-picker, #specific-end-date-picker, #specific-meeting-time-picker, #specific-num-of-slots").focus(function() {
		if($(this).hasClass("is-empty")) {
			$(this).removeClass("is-empty");
		}
	});
	
	// REMOVE SPECIFIC DATE
	$(document).on("click", ".delete-specific-date", function(e) {
		e.preventDefault();
		
		$(this).parent().remove();
	});
	
	// CANCEL UTILITY LIGHTBOX
	$("#light-box-utility-cancel, #light-box-utility-outer").click(function(e) {
		e.preventDefault();
		
		$("#light-box-utility-outer").fadeOut(function() {
			$("#specific-start-date-picker, #specific-end-date-picker, #specific-meeting-time-picker, #specific-num-of-slots").removeClass("is-empty")
			$("#specific-start-date-picker").val("");
			$("#specific-end-date-picker").val("");
			$("#specific-meeting-time-picker").val("");
			$(".specific-meeting-times").empty();
			$("#specific-num-of-slots").val("");
		});
	});
	$("#light-box-utility").click(function(e) {
		e.stopPropagation();
	});	
	
	// DELETE A SUBMISSION
	$(document).on("click", ".delete-kickoff", function(e) {
		e.preventDefault();
		var deleteButton = $(this);
		
		// CONFIRM BEFORE WE DELETE
		if(confirm("Are you sure you want to delete this? This can't be undone.") == true) {
			var recordID = $(this).attr("data-kickoff-id");
			
			app.database().ref("/formSubmissions/" + recordID).remove().then(function() {
				// SUCCESS
				successModal("The record was deleted!");
				deleteButton.parent().remove();
			}).catch(function(error) {
				// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
				lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The following error has occured:<br /><span style='color: #990000;'>" + error.code + ".</span><br />If you continue to receive this error, please contact " + contactNameEmail + " with the error details.</p>");
			});
		}
	});
	
	// TEST EMAIL ADDRESSES
	/*$("#test-emails").click(function(e) {
		e.preventDefault();
		
		// SET UP PROTOCOL AND DOMAIN
		var homeURL = location.protocol + "//" + window.location.hostname;
		
		// CALL THE PROXY SERVICE SO THAT WE CAN SEND THE EMAILS VIA EXTERNAL HTTP PAGE
		// HTTPS BLOCKS COOKIES, BROWSER STORAGE, CONTENT, AND IFRAMES, ETC THAT REFERENCE HTTP
		$.ajax({
			type: "POST",
			url: homeURL + "/GlobalAssets/Proxies/ServiceProxy.aspx/CallService", // PROXY SERVICE
			data: "{BaseURL: 'http://brentonkelly.me/Bb/scheduling-form/send-mail.php', InvokeMethod: 'GET', ServiceMethod: '', ServiceParameters: '?mailSubject=Creative Kick-Off Form Notification Email Test&mailBody=This is a test email sent to make sure that your email has been entered correctly in the Creative kick-off form settings&emails=" + $("#emails").val() + "', ConnectionType: '', RequestBody: ''}", // DATA OBJECT THAT INCLUDES THE URL AND PARAMETERS TO BE QUERIED
			async: true,
			contentType: "application/json; charset=utf-8", // THE RESPONSE IS IN JSON
			dataType: "json", // THE RESPONSE IS IN JSON
			cache: false,
			success: function(msg) {
				// PARSE THE RESPONSE STRING AS JSON
				var result = JSON.parse(msg.d);
				var data = JSON.parse(result.data);

				if($.trim(data.msg) == "success"){
					// SUCCESS
					lightBox("<h2>Sweet!</h2><p class='light-box-message'>The email notifications were sent successfully to all of the recipients. Please double check the email address(es) to be sure the test notification went through.</p>");
				} else {
					// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
					lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The following error has occured:<br /><span style='color: #990000;'>" + result + ".</span><br />If you continue to receive this error, please contact Brenton with the error details.</p>");
				}
			}
		});
	});*/
	
	// SAVE SETTINGS
	$("#settings-form").submit(function(e) {
		e.preventDefault();
		var adjustSpecificSlots = "";
		$("#specific-dates li").each(function() {
			adjustSpecificSlots += $(this).text();
			if(!$(this).is(":last-child")) {
				adjustSpecificSlots += ';';	
			}
		});
		
		var excludeDays = "";
		$("#excluded-dates li").each(function() {
			excludeDays += $(this).text();
			if(!$(this).is(":last-child")) {
				excludeDays += ',';	
			}
		});
		
		var meetingTimes = "";
		$(".meeting-times li").each(function() {
			var thisTime = slotTimesTwelveHour.indexOf($(this).text());
			meetingTimes += slotTimesTwentyFourHour[thisTime];
			if(!$(this).is(":last-child")) {
				meetingTimes += ',';	
			}
		});
		
		app.database().ref("/settings").update({
			"adjustSpecificSlots": adjustSpecificSlots,
			"daysOutToExpire": $.trim($("#days-to-expire").val()),
			"excludeDays": excludeDays,
			"futureDays": $.trim($("#days-into-future").val()),
			"meetingTimes": meetingTimes,
			"numberOfSlots": $.trim($("#num-of-slots").val())/*,
			"emails": $.trim($("#emails").val().replace(/\s/g, ""))*/
		}).then(function() {
			// SUCCESS
			successModal("Your settings have been updated!");
		}).catch(function(error) {
			// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
			lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The following error has occured:<br /><span style='color: #990000;'>" + error.code + ".</span><br />If you continue to receive this error, please contact " + contactNameEmail + " with the error details.</p>");
		});
	});
}

function lightBoxEvents() {
	// CLOSE LIGHTBOX
	$("#light-box-outer, #light-box-close").click(function(e) {
		e.preventDefault();
		$("#light-box-outer").fadeOut();
	});
	$("#light-box").click(function(e) {
		e.stopPropagation();
	});	
}

function authentication(db, action, username, password) {
	if(action == "login") {
		// SUBMIT THE EMAIL AND PASSWORD
		db.auth().signInWithEmailAndPassword(username + "@blackboard.com", password).then(function(authData) {
			// RELOAD THE PAGE
			//location.reload(true);
		}).catch(function(error) {
			// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
			lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The following error has occured:<br /><span style='color: #990000;'>" + error.code + ".</span><br />If you continue to receive this error, please contact " + contactNameEmail + " with the error details.</p>");
		});
	} else if(action == "logout") {
		// LOG OUT
		db.auth().signOut().then(function() {
			// RELOAD THE PAGE
			location.reload(true);
		});
	}
}

function lightBox(htmlMessage) {
	$("#light-box-outer #light-box-content").html(htmlMessage);
	$("#light-box-outer").fadeIn();	
}

function successModal(msg) {
	$(".notify.success").text(msg).show();
    setTimeout(function() {
    	$(".notify.success").fadeOut(function() {
        	$(this).empty();
        });
    }, 2500);
}