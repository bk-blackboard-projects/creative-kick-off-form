$(function() {
	// FIREBASE CONFIGURATION
	var creativeSchedulingApp = csFirebaseConfig("AIzaSyBsd5VapNbRM1W-rJpl-YRRyVpgPdA2L1A", "cs-kick-off-form.firebaseapp.com", "https://cs-kick-off-form.firebaseio.com", "");

	// CONNECT TO FIREBASE
	var kickOffDB = creativeSchedulingApp.database();

	// AUTHENTICATION
	creativeSchedulingApp.auth().onAuthStateChanged(function(user) {
    	if(user) {
			getData(kickOffDB, "settings", creativeSchedulingApp);
		}
    });
	
	creativeSchedulingApp.auth().signInAnonymously().catch(function(error) {
		// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
		lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The following error has occured:<br /><span style='color: #990000;'>" + error.code + ".</span><br />If you continue to receive this error, please contact Meghan Hawk (meghan.hawk@blackboard.com) with the error details.</p>");
	});
	
	CreativeCallController("/site/SiteController.aspx/SignIn", "{Username: 'cs.mru',Password: 'csmru'}",
	function(result) {}, // SUCCESS
	function(result) { // ERROR
		// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
		lightBox('<h2>Uh oh!</h2><p class="light-box-message">An error occured when trying to authenticate you. Please refresh the webpage and try again. If you continue to receive this error, please contact Meghan Hawk at meghan.hawk@blackboard.com.</p>');
	});
});

function csFirebaseConfig(apiKey, authDomain, databaseURL, storageBucket) {
	// FIREBASE CONFIGURATION
	var config = {
		apiKey: apiKey,
		authDomain: authDomain,
		databaseURL: databaseURL,
		storageBucket: storageBucket
	};
	
	// INITIALIZE THE FIREBASE
	var csFirebaseApp = firebase.initializeApp(config);
	
	// RETURN THE FIREBASE INSTANCE
	return csFirebaseApp;
}

function getData(kickOffDB, view, app) {
	// BUILD THE VIEW AND SET AN ASYNCHRONOUS CALLBACK TO KEEP VIEW IN SYNC WITH DATA AT ALL TIMES
	kickOffDB.ref().on("value", function(snapshot) {
		// REBUILD THE FORM WITH THE NEW DATA
		buildView(kickOffDB, snapshot, app);
		
		// ADD DEFAULT UI EVENTS
		uiEvents(kickOffDB, snapshot, app);
	}, function(error) {
		if(error) {
			// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
			lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The following error has occured:<br /><span style='color: #990000;'>" + error.code + ".</span><br />If you continue to receive this error, please contact Meghan Hawk (meghan.hawk@blackboard.com) with the error details.</p>");
		}
	});
}

function buildView(kickOffDB, snapshot) {
	// GET THE SUBMISSIONS
	var kickOffs = snapshot.child("/formSubmissions").val();
	
	// GET THE SETTINGS
	var settings = snapshot.child("/settings").val();
	var futureDays = settings.futureDays;
	
	// SET UP OUR VARIABLES
	var currTime = new Date();
	var loopDate = moment.tz(currTime.getTime(), "America/New_York"); // CURRENT EASTERN TIME
	loopDate.add(parseInt(settings.daysOutToExpire), "days"); // SET LOOPDATE TO START ON PROPER DAY
	var rightNowET = moment.tz(currTime.getTime(), "America/New_York"); // CURRENT EASTERN TIME
	rightNowET.add(parseInt(settings.daysOutToExpire), "days"); // RIGHT NOW IS ACTUALLY THE TIME NOW BUT ON THE DAY THE TIMES ARE SET TO START
	var futureDay = moment(rightNowET).add(futureDays, "days");
	var slotTimesTwelveHour = ["8:00 AM", "9:00 AM", "10:00 AM", "11:00 AM", "12:00 PM", "1:00 PM", "2:00 PM", "3:00 PM", "4:00 PM", "5:00 PM"];
	var slotTimesTwentyFourHour = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
	
	// CREATE A BANK OF THE DATES THAT ARE ALREADY SCHEDULED
	var scheduledDates = [];
	for(var i in kickOffs) {
		scheduledDates.push(kickOffs[i].date);
	}
	
	// CREATE A BANK OF EXCLUDED DATES
	var excludedDatesList = settings.excludeDays.split(","), excludedDates = [], excluededDate = "";
	for(var d in excludedDatesList) {
		excludedDate = new Date(excludedDatesList[d]);
		excludedDates.push(moment.tz(excludedDate, "America/New_York").format("YYMMDD"));
	}
	
	// CREATE A BANK OF TIME SLOTS
	var timeSlotList = settings.meetingTimes.split(","), regularTimeSlots = [];
	for(var t in timeSlotList) {
		regularTimeSlots.push(parseInt(timeSlotList[t]));
	}
	
	// CREATE A BANK OF SPECIFIC DATE RANGES, TIMES, AND SLOTS
	if(settings.adjustSpecificSlots != "") {
		var specificSlotsList = settings.adjustSpecificSlots.split(";"), specificSlots = [], allSpecificSlotDates = [];;
		
		// LOOP THROUGH EACH ENTRY
		for(var s in specificSlotsList) {
			var specificSlotProps = specificSlotsList[s].split("|");
			
			// GET SLOT NUMBER FOR THE ENTRY AND SET UP THE ENTRY OBJECT
			var specificSlotNum = parseInt($.trim(specificSlotProps[2].replace("Number of Time Slots:", "")));
			var specificSlotEntry = {"dateRange": [],"timeSlots": [],"slotNum": specificSlotNum};

			// GET DATES IN DATE RANGE FOR THE ENTRY
			var specificSlotDateRange = specificSlotProps[0].split("-");
			var specificStartDate = new Date($.trim(specificSlotDateRange[0]));
			var specificEndDate = new Date($.trim(specificSlotDateRange[1]));
			var specificFormattedStartDate = moment.tz(specificStartDate.getTime(), "America/New_York");
			var specificFormattedEndDate = moment.tz(specificEndDate.getTime(), "America/New_York");

			while(specificFormattedStartDate <= specificFormattedEndDate) {
				specificSlotEntry.dateRange.push(specificFormattedStartDate.format("YYMMDD"));
				allSpecificSlotDates.push(specificFormattedStartDate.format("YYMMDD"));
				
				specificFormattedStartDate.add(1, "days");
			}
			
			// GET THE TIME SLOTS FOR THE ENTRY
			var specificSlotTimes = $.trim(specificSlotProps[1].replace("Time Slots:", "")).split(",");
			for(var i in specificSlotTimes) {
				var thisTime = slotTimesTwelveHour.indexOf($.trim(specificSlotTimes[i]));
				
				specificSlotEntry.timeSlots.push(slotTimesTwentyFourHour[thisTime]);
			}
			
			// PUSH THIS ENTRY TO THE MAIN ARRAY
			specificSlots.push(specificSlotEntry);
		}
	}
	
	// GET THE CURRENTLY SELECTED DATE/TIME IF THERE IS ONE
	var currentSelectedDate = "";
	if($("#dates option:selected").length && $("#dates option:selected").val() != "loading") {
		currentSelectedDate = $("#dates option:selected").val();
	}

	// BUILD THE AVAILABLE DATES
	var availableDates = "", timeET = "", scheduledOccurrences = 0, timeSlots = "", slotNum = 0;
	while(loopDate <= futureDay) {
		// EXCLUDE WEEKENDS AND EXCLUDED DATES
		if(loopDate.day() != 0 && loopDate.day() != 6 && excludedDates.indexOf(loopDate.format("YYMMDD")) == -1) {
			// CHECK TO SEE IF THERE ARE SPECIFIC TIME SLOTS FOR THIS DATE
			if(settings.adjustSpecificSlots != "" && allSpecificSlotDates.indexOf(loopDate.format("YYMMDD")) > -1) {
				// LOOP THROUGH THE SPECIFIC SLOTS ARRAY TO LOCATE THE SPECIFIC ENTRY
				for(var entry in specificSlots) {
					// IF THE DATE HAS BEEN LOCATED
					if(specificSlots[entry].dateRange.indexOf(loopDate.format("YYMMDD")) > -1) {
						// SET TIME SLOTS AND SLOT NUMBER
						timeSlots = specificSlots[entry].timeSlots;
						slotNum = specificSlots[entry].slotNum;
					}
				}
			} 
			
			// THIS IS JUST A REGULAR DATE
			else {
				// SET TIME SLOTS AND SLOT NUMBER
				timeSlots = regularTimeSlots	;
				slotNum = settings.numberOfSlots;
			}
			
			// LOOP THROUGH THE TIME SLOTS
			for(var time in timeSlots) {
				// TIMESTAMP IN EASTERN TIME
				timeET = loopDate.startOf('day').hour(timeSlots[time]).minute(0);
				
				// CHECK TO MAKE SURE TIME IS NOT IN THE PAST - WILL ONLY MATTER FOR TODAYS TIMES
				if(timeET > rightNowET) {
					// CLEAR OUT THE OCCURENCES ARRAY
					scheduledOccurrences = 0;
					
					// POPULATE OCCURANCES FOR THIS DATE AND TIME
					for(var scheduledDate in scheduledDates) {
						if(scheduledDates[scheduledDate].indexOf(timeET.format("dddd, MMMM D @ h:mm A")) > -1) {
							scheduledOccurrences++;
						}
					}
					
					// CHECK TO SEE IF THIS TIME SLOT IS MAXED OUT
					if(slotNum - scheduledOccurrences > 0) {
						availableDates += '<option value="' + timeET.format("dddd, MMMM D @ h:mm A") + ' ET" data-epoch-time="' + timeET + '">' + timeET.format("dddd, MMMM D @ h:mm A") + ' Eastern Time</option>';
					}
				}
			}
		}
		
		// ADD 1 DAY TO LOOP DATE FOR THE NEXT LOOP
		loopDate.add(1, "days");
	}
	
	// ADD THE DATES TO THE DOM
	$("#dates").html(availableDates);
	
	// CHECK TO SEE IF THERE IS A USER CURRENTLY FILLING OUT THE FORM THAT HAS A DATE SELECTED THAT IS NO LONGER AVAILABLE
	// THE INTERFACE UPDATES LIVE EVERY TIME A USER SUBMITS THE FORM
	if(currentSelectedDate != "") {
		if(!$("#dates option[value='" + currentSelectedDate + "']").length) {
			// SHOW LIGHTBOX MESSAGE
			lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The date and time you chose is no longer available! Please choose another date and time for your meeting.</p>");
		} else {
			$("#dates option[value='" + currentSelectedDate + "']").attr("selected", "selected");	
		}
	}
}

function uiEvents(kickOffDB, snapshot, app) {
	$("#kick-off-form, .form-input, #light-box-outer, #light-box-close, #light-box").unbind();
	
	// SUBMIT THE FORM
	$("#kick-off-form").submit(function(e) {
			e.preventDefault();
			$(".form-input.is-empty").removeClass("is-empty");
			
			// DO SOME VALIDATION
			var hasError = false, valErrors = "";
			$(".form-input.required").each(function() {
				if($.trim($(this).val()) == "" || $.trim($(this).val()) == $(this).attr("data-error-message")) {
					$(this).addClass("is-empty");
					
					valErrors += '<li>' + $(this).attr("data-error-message") + '</li>';
					hasError = true;
				}
			});
			
			if(!hasError) {
				var chosenDateTime = $("#dates option:selected").val();
			
				// SAVE THE SUBMISSION TO THE DATABASE
				app.database().ref("/formSubmissions").push({
					"fullName": $.trim($("#full-name").val()),
					"email": $.trim($("#email").val()),
					"organization": $.trim($("#organization").val()).replace(/#/, ""),
					"notes": $.trim($("#notes").val()),
					"date": chosenDateTime,
					"epochTime": $("#dates option:selected").attr("data-epoch-time")
				}).then(function() {
				
					var notes = ($.trim($("#notes").val()) == "") ? "None provided" : $.trim($("#notes").val());

					SendEAlert({
						"Name": CreativeAddSlashes($.trim($("#full-name").val())),
						"Email": CreativeAddSlashes($.trim($("#email").val())),
						"DistrictName": CreativeAddSlashes($.trim($("#organization").val())),
						"DateTime": CreativeAddSlashes(chosenDateTime),
						"Notes": CreativeAddSlashes(notes),
						"Success": function(result) {
							lightBox('<h2>Thank You!</h2><p class="light-box-message">Thank you for registering for a Schoolwires Template Kick-Off Meeting. You will be receiving a template kick-off meeting confirmation within the next 3 business days.</p>');
						},
						"Error": function(result) {
							lightBox('<h2>Uh oh!</h2><p class="light-box-message">An error occured when trying to save your meeting request. Please refresh the webpage and try again. If you continue to receive this error, please contact Meghan Hawk at meghan.hawk@blackboard.com.</p>');
						}
					});

					// CLEAN OUT THE INPUTS
					$(".form-input").each(function() {
						$(this).val("");
					});
					$("#dates option:selected").removeAttr("selected");
					$("#dates option:first-child").attr("selected", "selected");
					
				}).catch(function(error) {
					// SHOW A USEFUL ERROR MESSAGE IF AN ERROR OCCURS
					lightBox("<h2>Uh oh!</h2><p class='light-box-message'>The following error has occured:<br /><span style='color: #990000;'>" + error.code + ".</span><br />If you continue to receive this error, please contact Meghan Hawk (meghan.hawk@blackboard.com) with the error details.</p>");
				});
			} else {
				// SHOW ERROR MESSAGE
				lightBox("<h2>Uh oh!</h2><ul class='light-box-message'>" + valErrors + "</ul>");	
			}
	});
	
	// FORM INPUT FOCUS
	$(".form-input").focus(function() {
		if($.trim($(this).val()) == "" || $.trim($(this).val()) == $(this).attr("data-error-message")) {
			$(this).val("").removeClass("is-empty");
		}
	});
	
	// CLOSE LIGHTBOX
	$("#light-box-outer, #light-box-close").click(function(e) {
		e.preventDefault();
		$("#light-box-outer").fadeOut();
	});
	$("#light-box").click(function(e) {
		e.stopImmediatePropagation();
	});
}

function lightBox(htmlMessage) {
	$("#light-box-outer #light-box-content").html(htmlMessage);
	$("#light-box-outer").fadeIn();	
}

function SendEAlert(props) {
	var data = "{" +
		"DomainID: 317, " +
		"SenderName: 'Creative Services Meeting Request', " + 
		"SenderEmail: 'noreply@blackboard.com', " + 
		"Subject: 'A Creative Kick-Off Has Been Scheduled', " +
		"Body: '" + props.Name + " (" + props.Email + ") from " + props.DistrictName + " has scheduled a kick-off meeting for " + props.DateTime + ".\n\nNotes: " + props.Notes + "'" +
	"}";

	CreativeCallController("/cms/UserControls/ContentAlert/ContentAlertController.aspx/InsertContentAlert", data, props.Success, props.Error);
}

function CreativeCallController(URL, Data, SuccessCallback, FailureCallback, IsSynchronous) {
    if (URL.URL) {
        Data = URL.Data;
        SuccessCallback = URL.SuccessCallback;
        FailureCallback = URL.FailureCallback;
        URL = URL.URL;
    }
   
    $.ajax({
        type: "POST",
        url: URL,
        data: Data,
        async: (IsSynchronous == 1 ? false : true),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function(msg) {
            var result = JSON.parse(msg.d);

            if (result[0].success == "true") {
                SuccessCallback(result);
            } else {
                FailureCallback(result);
            }
        }
    });
}

//Add slashes on insert and update for quotes, double quotes, and backslashes
function CreativeAddSlashes(str) {
    if (str !== undefined) {
        str = str.replace(/\\/g, '\\\\'); // this one must go before single and double quotes
        str = str.replace(/\'/g, '\\\'');
        str = str.replace(/\"/g, '\\\"');

        return str;
    } else {
        return false;
    }
}
